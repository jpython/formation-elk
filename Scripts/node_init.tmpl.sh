#!/usr/bin/env bash

install -d -m 700 /root/.ssh
base64 -d > /root/.ssh/authorized_keys <<EOF
PUBKEY
EOF

apt -y update
apt -y install apt-transport-https

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt update && sudo apt install elasticsearch
systemctl daemon-reload 
systemctl enable elasticsearch.service 
systemctl start elasticsearch.service 

apt install kibana
systemctl enable kibana.service 
systemctl start kibana

date > /tmp/cloud-init-node-ok

