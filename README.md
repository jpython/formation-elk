# ELK on AWS EC2 

Deploy ELK (Elastic Search) on EC2 instances

# Deploy EC2 virtual machines with Terraform

## Configure your aws local user configuration

You can create a specific AWS user on AWS Web Console with full admin privileges
and use this user (i.e. the corresponding access key and secret access key) later
on. Then you could delete this user at the end of this hands on exercices.

Create your `.aws` personal configuration directory where you will put two files.

`~/.aws/credentials`

```
[default]
aws_access_key_id=YYRR(YOUR ACCESS KEY)UUU
aws_secret_access_key=887AA(YOUR SECRET ACCESS KEY)UUY667
``` 

Make sure only YOU can read that file: `chmod a=rw,go= ~/.aws/credentials`

`~/.aws/config`

```
[default]
region=us-east-2c
output=json
```

We are suggesting us-east-2c as the AMI we will be using is only available there, anyway
it is very likely than any reasonable Debian 9/10 AMI, or a recent CentOS will do (you
may have to adapt Terraform files in this case)

In addition you do have to accept terms and subcribe to the AMI you intend to use
in AWS Web Console (ami-0788cc5a8c3155f0f if you do not change instance.tf).

## Install and run Terraform

Install terraform (last version, or v0.12.18 this procedure has been tested with) and
put terraform binary in a directory which is in your PATH.

You can install a version of Terraform compatible with this deploiement :

```
$ wget https://releases.hashicorp.com/terraform/0.12.19/terraform_0.12.19_linux_amd64.zip
$ unzip terraform_0.12.19_linux_amd64.zip
$ sudo mv terraform /usr/local/bin
```

In addition we need jq which is used by the bastion login script.

Get the files from this repository, build files and initialize Terraform, then construct
your EC2 infrastructure:

```
sudo apt install jq
git clone https://framagit.org/jpython/formation-elk.git
cd formation-elk/
cd ssh-keys/
./generate_keys 
cd ../Scripts/
make
cd ../Tf/
terraform init
terraform plan
terraform apply
```

If every went well you should be able to connect to your bastion ec2 instance:

```
$ ./go_bastion
Bastion public ip: 3.15.139.236
Warning: Permanently added '3.15.139.236' (ECDSA) to the list of known hosts.
Linux ip-10-0-0-42 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64
...
admin@ip-10-0-0-42:~$ 
```

If it doesn't work you can check your security group in `instances.tf`:

If you have stopped then started your instance its public ip is likely to
have changed, you can ask Terraform to refresh its state:
~~~~
$ terraform refres
~~~~

```
resource "aws_security_group" "sg_bastion" {
  name = "sg_bastion"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["YOURPUBLICIP/32"]
...
```

You can fill up with your local network public ip or (it is safe as
ssh is configured to refuse password authentication on our AMI)
`0.0.0.0/0` to allow SSH access from everywhere.

Then re-apply Terraform plan:
```
terraform apply
```

You first have to wait for cloud-init to have finished, at that time a
file `/tmp/cloud-init-bastion-ok` will appear. If you get bored you can
run this waiting for the cloud-init process to disappear :

```
admin@ip-10-0-0-42:~$ watch pstree
```

Then you'll have to wait (again!) for all nodes to be properly setted up:

```
admin@ip-10-0-0-42:~$ watch ./check_node_ci 50 100 [... other workers] 
```

When you'll see a file `/tmp/cloud-init-node-ok` at all nodes you can move forward.

## Install Elastic Search on one node

A few steps will make you install Elastic Search on first node

```
$ wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
$ echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
$ sudo apt-get update && sudo apt-get install elasticsearch
$ sudo systemctl daemon-reload 
$ sudo systemctl enable elasticsearch.service 
$ sudo systemctl start elasticsearch.service 
$ sudo journalctl --unit elasticsearch
```

You can check that Elastic Search is running by GETting resource
`/` on a HTTP connection on local port 9200 :

```
$ curl --noproxy "*" http://localhost:9200/
```

The answer should look like this:

```JSON
{
  "name" : "ip-10-0-0-50",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "oTvASfArRF-1vf83cCxd9g",
  "version" : {
    "number" : "7.5.1",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "3ae9ac9a93c95bd0cdc054951cf95d88e1e18d96",
    "build_date" : "2019-12-16T22:57:37.835892Z",
    "build_snapshot" : false,
    "lucene_version" : "8.3.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

## Install Kibana (Web Interface)
```
$ sudo apt install kibana
$ sudo systemctl enable kibana.service 
$ sudo systemctl start kibana
$ sudo systemctl status kibana
```

A reverse proxy routing HTTPS authenticated connection to Kibana is
better but if you are in a hurry you can setup a ssh tunnel to
access Kibana form the outside word.

Make sure that your security group `sg_bastion` refer to
your public IP adress (the one of the place you are currently
working) :

```
resource "aws_security_group" "sg_bastion" {
  name = "sg_bastion"
  ...
  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
    cidr_blocks = ["YOUR PUBLIC IP ADDRESS/32"] # 
  }
  ...
```

To be able to connect to Kibana Web server you can connect
to your bastion then create a SSH encrypted tunnel to it:

```
admin@ip-10-0-0-42:~$ sudo ssh -L *:80:127.0.0.1:5601 10.0.0.50 -f -N
```

On your local station you can connect to Kibana with a Web navigator
at the url: http://public_ip_of_bastion/

_This IP can be retrieve from your station with:_

```
terraform output -json bastion_ip | jq -r '.[0]'
```

## A more secure way to connect to Kibana from the outside

It is far more secure to access Kibana through a reverse Web
proxy accepting HTTPS (encrypted) and authenticated connection
then relay them to Kibana Web Server.

Nginx (or Apache) is perfect for that job! You can install it
on any node with a public IP adress, here we will do it on the
bastion virtual machine.

Connect to the bastion and install nginx and a few tools. If the
SSH tunnel is up you will have to kill it first (it is using port
80/TCP):
```
$ ./go_bastion
admin@ip-10-0-0-42:~$ ps aux | grep ssh
...
root     NNNNN  0.0  0.2  46756  2880 ?        Ss   13:57   0:00 ssh -L *:80:127.0.0.1:5601 10.0.0.50 -f -N
...
$ sudo kill NNNN
$ sudo apt install nginx openssl apache2-utils
```

Edit as root the configuration file `/etc/nginx/nginx.conf` so that it looks
like:
```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
	# multi_accept on;
}

http {
  upstream realsrv {
          server 10.0.0.50:5601 weight=1;
  }
  server {
    listen 443 ssl;
    listen [::]:443 ssl;
    include ./self-signed.conf;
    include ./ssl-params.conf;

    server_name me.mydomain.org;

    location / {
       auth_basic            "Username and Password Required";
       auth_basic_user_file  /etc/nginx/.htpasswd;
       proxy_pass http://realsrv;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
     } 
  }

  server {
    listen 80;
    listen [::]:80;

    server_name me.mydomain.org;

    return 302 https://$server_name$request_uri;
  }
}
```

And create as root two auxiliary configuration files for SSL:

`/etc/nginx/self-signed.conf` :

~~~~
ssl_certificate /etc/nginx/ssl/nginx-selfsigned.crt;
ssl_certificate_key /etc/nginx/ssl/private/nginx-selfsigned.key;
~~~~

`/etc/nginx/ssl-params.conf` : 

~~~~
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/nginx/ssl/dhparam.pem;
ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
~~~~

Create the certificate files:

```
admin@ip-10-0-0-42:~$ sudo mkdir -p /etc/nginx/ssl/private
admin@ip-10.0.0-42:~$ sudo chmod a=rwx,go= /etc/nginx/ssl/private
admin@ip-10-0-0-42:~$ cd /etc/nginx/ssl
admin@ip-10-0-0-42:~$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout private/nginx-selfsigned.key \
    -out ./nginx-selfsigned.crt

...
Generating a RSA private key
......................+++++
..........................+++++
writing new private key to 'private/nginx-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:Bretagne
Locality Name (eg, city) []:Brest
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Flying Circus
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:me.mydomain.org
Email Address []:contact@mydomain.org

$ sudo openssl dhparam -out dhparam.pem 2048
```

Preparing the authentication user file for Nginx and set the
kibana user's password:
```
admin@ip-10-0-0-42:~$ sudo htpasswd -c /etc/nginx/.htpasswd kibana
```

Here is the complete NGINX configuration layout:
~~~~
$ tree /etc/nginx
.../nginx
└── etc
    ├── nginx
    │   ├── nginx.conf
    │   ├── self-signed.conf
    │   └── ssl-params.conf
    └── ssl
        ├── dhparam.pem
        ├── nginx-selfsigned.crt
        └── private
            └── nginx-selfsigned.key
~~~~ 

You can restart nginx then:
```
dmin@ip-10-0-0-42:~$ sudo systemctl restart nginx
``` 

On the node which is running Kibana (10.0.0.50), ask Kibana to listen
on the VPC interface, i.e. edit `/etc/kibana/kibana.yml`:
~~~~
#server.host: "localhost" 
server.host: "10.0.0.50"
~~~~

and restart Kibana :
~~~~
admin@10.0.0.50:~$ sudo systemctl restart kibana
~~~~

You can test by going to url https://your_public_ip/ with a Web navigator.


