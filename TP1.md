Le script `tunnel_to_es` construit un double ssh à partir de notre station de
travail vers le bastion puis le serveur ElasticSearch, on peut donc exécuter
des call API en curl/JSON directement à partir de notre poste de travail,
comme ES tournait sur notre machine.

~~~~
$ apt install curl # yum install curl / brew install curl
$ ./tunnel_to_es
$ curl http://localhost:9200/
{
  "name" : "ip-10-0-0-50",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "Ne6X5h41R_ytFgYzYS7K9g",
  "version" : {
    "number" : "7.5.1",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "3ae9ac9a93c95bd0cdc054951cf95d88e1e18d96",
    "build_date" : "2019-12-16T22:57:37.835892Z",
    "build_snapshot" : false,
    "lucene_version" : "8.3.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
~~~~

On peut dérouler les exercices ci-dessous à partir de notre station
de travail et expérimenter d'autres moyen de faire des appels à
l'API ES :

- JSON (syntaxe Lucene)
   - avec curl (ou wget)
   - avec une extension REST du navigateur
     (Chrome ou Firefox)
- Quasi SQL (à tester)
- Call via une librairie Python ou Java (ou…)
- Call en KQL (nouveau en ES 7)

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474691-etudiez-le-fonctionnement-d-elasticsearch

Pour démarrer les différents étape du TP on crée un répertoire de travail et on y copie le fichier 
`movies_for_elastic.zip` récupéré avec notre navigateur Web
~~~
$ mkdir Movies
$ cd Movies
$ cp ~/Téléchargements/movies_for_elastic.json.zip .
$ unzip movies_for_elastic.json.zip 
$ curl -XPUT localhost:9200/_bulk -H"Content-Type: application/json" --data-binary @movies_elastic.json
~~~

Pour revenir à zéro on peut effacer un index (i.e. une base de donnée déjà importée)
~~~~
$ curl http://localhost:9200/_cat/indices
...
yellow open movies                     lOPaJ7SMQyOASjMsH9HxrA 1 1  4849 0  4.7mb  4.7mb
...
$ curl -XDELETE 'localhost:9200/movies'
~~~~

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474696-interrogez-des-donnees-textuelles

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474701-faites-grandir-votre-base

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4686486-visualisez-et-prototypez-avec-kibana

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/6735351-entrainez-vous-a-extraire-lessence-dune-base-de-donnees
