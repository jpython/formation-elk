#!/usr/bin/env python3

from  elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q, A

#es = Elasticsearch(['http://localhost:9200/'])

# la connextion à l'API localhost:9200
es = Elasticsearch()

# Build a DSL Search object on the 'commits' index, 'summary' document type
# constuire une requête
# sur la connexion es, dans l'index movies2 avec un filtrage sur
# le type de document
request = Search(using=es, index='movies2',
                 doc_type='movie')

# Run the Search, using the scan interface to get all resuls
# scan : récupère tout / execute : récupère 10 valeur (size/from)
response = request.scan()
# renvoie un itérateur qui contient des "hits"
# hit a un champ fields qui a des champs qui sont les attributs
for hit in response:
    break
    print(hit.fields.title, end=' ')
    if hasattr(hit.fields, "directors"):
        print("(", ' '.join(hit.fields.directors), ")", sep='')
    else:
        print()

print("Seconde recherce :")
#exit(0)
s = Search(using=es,index='movies2',doc_type='movie') \
        .filter("exists", field="fields.plot") \
        .query("match", df="fields", plot="Wars") \
#        .query("match", **{ 'fields.plot': 'Wars' }) \
#        .exclude("match", **{ 'fields.title': 'Star Wars'} )


r = s.scan()
for hit in r:
    print(hit.fields.title)
    print(hit.fields.directors)
