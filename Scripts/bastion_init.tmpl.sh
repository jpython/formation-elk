#!/usr/bin/env bash

# APT 
apt -y update
apt -y upgrade
apt -y install squid
apt -y install dirmngr
apt -y install git
sed --in-place -e '/^http_access deny all/d' /etc/squid/squid.conf
cat >> /etc/squid/squid.conf <<EOF
acl allowedips src 10.0.0.0/24
http_access allow allowedips
http_access deny all
forwarded_for off
cache_dir ufs /var/spool/squid 100 16 256
cache_mem 16 MB
maximum_object_size 15 MB
http_port 3128 transparent
EOF
systemctl restart squid

# Quick and not that dirty
install -d -m 700 /root/.ssh
base64 -d > /root/.ssh/id_rsa <<EOF
PRIVKEY
EOF

install -d -m 700 -u admin -g admin /home/admin/.ssh
base64 -d > /home/admin/.ssh/id_rsa <<EOF
PRIVKEY
EOF

chmod -R u+rwX,go= /root/.ssh
chmod -R u+rwX,go= /home/admin/.ssh
chown -R admin:admin /home/admin/.ssh

cat > /home/admin/check_node_ci <<'EOF'
#!/usr/bin/env bash

for i in $*
do
  echo -en "Node : 10.0.0.${i}\t"
  ssh -o StrictHostKeyChecking=no 10.0.0.$i "ls /tmp/cloud-init* 2> /dev/null" 2> /dev/null
  echo
done
EOF

chown admin:admin /home/admin/check_node_ci
chmod +x admin:admin /home/admin/check_node_ci

date > /tmp/cloud-init-bastion-ok
